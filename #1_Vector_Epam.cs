// First task
using System;
using System.Text;

namespace EpamTasks
{
	struct Vector
	{
		private double _x;
		private double _y;
		private double _z;
		
		public Vector(double x, double y, double z)
		{
			_x = x;
			_y = y;
			_z = z;
		}
		
		public double Length
		{
			get
			{
				return Math.Sqrt(Math.Abs(_x * _x + _y * _y + _z * _z));
			}
		}
		
		public static Vector CrossProduct(Vector vector1, Vector vector2)
		{
			return new Vector(vector1._y * vector2._z - vector1._z * vector2._y, vector1._x * vector2._z - vector1._z * vector2._x, vector1._y * vector2._x - vector1._x * vector2._y);
		}
		
		public static double TripleProduct(Vector vector1, Vector vector2, Vector vector3)
		{
			return vector1 * Vector.CrossProduct(vector2, vector3);
		}
		
		public static double AngleBetween(Vector vector1, Vector vector2)
		{
			return Math.Acos((vector1 * vector2) / (vector1.Length * vector2.Length));
		}
		
		public override bool Equals(object obj)
		{
			// If parameter is null return false to avoid unnecessary step with creating new vector and "as"
			if (obj == null)
			{
				return false;
			}

			Vector other = (Vector)obj;

			if (other == null)
			{
				return false;
			}

			return this._x == other._x && this._y == other._y && this._z == other._z;
		}
		
		// Equals for own types to improve performance
		public bool Equals(Vector other)
		{

			if ((object)other == null)
			{
				return false;
			}

			return this._x == other._x && this._y == other._y && this._z == other._z;
		}
		
		public static bool Equals( Vector vector1, Vector vector2)
		{
			// If both are null, or both are same instance, return true
			if (Object.ReferenceEquals(vector1, vector2))
			{
				return true;
			}

			// If one is null, but not both, return false.
			if (((object)vector1 == null) || ((object)vector2 == null))
			{
				return false;
			}

			// Return true if the fields match.
			return vector1._x == vector2._x && vector1._y == vector2._y && vector1._z == vector2._z;
		}
		
		public override int GetHashCode()
		{
			return (int)_x ^ (int)_y ^ (int)_z;
		}
		
		public static Vector Max(Vector vector1, Vector vector2)
		{
			return (vector1 > vector2 ? vector1 : vector2).Copy();
		}
		
		public static Vector Min(Vector vector1, Vector vector2)
		{
			return (vector1 < vector2 ? vector1 : vector2).Copy();
		}
		
		public Vector Copy()
		{
			return new Vector(_x, _y, _z);
		}
		
		public override string ToString()
		{
			return String.Format("({0}; {1}; {2})", _x, _y, _z);
		}
		
		public static Vector Add(Vector vector1, Vector vector2)
		{
			return new Vector(vector1._x + vector2._x, vector1._y + vector2._y, vector1._z + vector2._z);
		}
		
		public static Vector Subtract(Vector vector1, Vector vector2)
		{
			return new Vector(vector1._x - vector2._x, vector1._y - vector2._y, vector1._z - vector2._z);
		}
		
		public static double Multiply(Vector vector1, Vector vector2)
		{
			return vector1._x * vector2._x + vector1._y * vector2._y + vector1._z * vector2._z;
		}
		
		public static Vector operator +(Vector vector1, Vector vector2)
		{
			return new Vector(vector1._x + vector2._x, vector1._y + vector2._y, vector1._z + vector2._z);
		}
		
		public static Vector operator -(Vector vector1, Vector vector2)
		{
			return new Vector(vector1._x - vector2._x, vector1._y - vector2._y, vector1._z - vector2._z);
		}
		
		public static double operator *(Vector vector1, Vector vector2)
		{
			return vector1._x * vector2._x + vector1._y * vector2._y + vector1._z * vector2._z;
		}
		
		public static bool operator ==(Vector vector1, Vector vector2)
		{
			return Equals(vector1, vector2);
		}

		public static bool operator !=(Vector vector1, Vector vector2)
		{
			return !Equals(vector1, vector2);
		}
		
		public static bool operator >(Vector vector1, Vector vector2)
		{
			return vector1.Length > vector2.Length;
		}
		
		public static bool operator <(Vector vector1, Vector vector2)
		{
			return vector1.Length < vector2.Length;
		}
	}
	
	class EntryPoint
	{
		static void Main()
		{
			var vec1 = new Vector(1.3, 2.0, 3.2);
			var vec2 = new Vector(4.2, 5.1, 6.7);
			var vec3 = new Vector(1.3, 2.0, 3.2);
			Console.WriteLine("\n******************************FIRST TASK*****************************\n");
			Console.WriteLine("First vector: {0}", vec1);
			Console.WriteLine("Second vector: {0}", vec2);
			Console.WriteLine("Third vector: {0}\n", vec3);
			
			Console.WriteLine("First + second = {0}", vec1 + vec2);
			Console.WriteLine("First - second = {0}", vec1 - vec2);
			Console.WriteLine("First * second = {0}", vec1 * vec2);
			Console.WriteLine("Cross product of first and second = {0}", Vector.CrossProduct(vec1, vec2));
			Console.WriteLine("Triple product of first, second and third = {0}", Vector.TripleProduct(vec1, vec2, vec3));
			Console.WriteLine("Length of first = {0}", vec1.Length);
			Console.WriteLine("Angle between first and second = {0}", Vector.AngleBetween(vec1, vec2));
			Console.WriteLine("Bigger between the first and second = {0}", Vector.Max(vec1, vec2));
			
			if (vec1 == vec3)
			{
				Console.WriteLine("First and third are equals");
			}
			if (vec1 != vec2)
			{
				Console.WriteLine("First and second are not equals");
			}
			if (vec1 > vec2)
			{
				Console.WriteLine("First is larger than second");
			}
			if (vec1 < vec2)
			{
				Console.WriteLine("Second is larger than first");
			}
		}
	}
}